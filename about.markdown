---
layout: default
title: About
permalink: /about/
---

## About NiSERCast

<img id="about-cover" src="{{ site.data.podcast.cover }}">

In this podcast we invite professors at NISER to talk candidly about their
work, and the passion which drives it. 
We try to convey what life in academia is like, and what it's like being an
academic leading some of the most important research in the country.
This podcast is for laymen, high-school students, undergrads, and anyone else
who is interested in how research is done, and how science is practised here.

NiSERCast is brought to you by a small group of students as a part of
NISER Science Activities Club's outreach efforts,
and is released under a [Creative Commons
Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/).

## About NISER

National Institute of Science Education and Research (NISER) is an institute
located in Bhubaneswar, Odisha which aims to
[train and nurture human resources in the Sciences for the knowledge economies of the future. ↗](https://www.niser.ac.in)

## About the website

Web design by [Ayush Singh](https://11de784a.github.io/). 
Cover art design by [Spandan Anupam](https://github.com/surelynottrue) and
K Prahlad Narasimhan.
We extend our thanks to [these people from thenounproject](https://gitlab.com/nisercast/nisercast.gitlab.io/-/snippets/2099948) for their design contributions.
