const nisercast = document.querySelector('header h1');
const hamburger = document.querySelector('#hamburger');
const hambimg = document.querySelector('#hamburger img');
const navul = document.querySelector('nav ul');

hamburger.addEventListener('click', e => { 
	if (navul.style.display == 'none') {
		navul.style.display = 'unset';
		nisercast.style.display = 'none';
		hambimg.style.transform = 'rotate(90deg)';
	} else {
		navul.style.display = 'none';
		nisercast.style.display = 'unset';
		hambimg.style.transform = 'unset';
	}
});
