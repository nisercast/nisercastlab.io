---
title: Home
layout: default
---

## Recent Episodes

{% for post in site.posts %}
<section>
  <h3><a href="{{ post.url }}">{{ post.title }} {% if post.guest %} with {{
  post.guest }} {% endif %}</a></h3>
  <p class="date">{{ post.date | date: "%B %d, %Y"}}</p>
  <audio controls preload='auto' style='width: 100%;'><source src='{{ post.audio | relative_url }}'></audio>
  {{ post.excerpt }}
</section>
{% endfor %}
