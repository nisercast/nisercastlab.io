---
layout: post
title: "#1: A Story for Everything"
guest: "Prof. Varadharajan Muruganandam"
date: 2021-04-19 07:00:00 +0530
audio: /assets/episodes/001.mp3
type: audio/mpeg
length: 4380 # duration of episode in seconds
---

Our guest in this first episode of NiSERCast is [Prof. Varadharajan 
Muruganandam](http://www.niser.ac.in/sms/professor/vmuruganandam), from the School of Mathematical Sciences at NISER. In this 
episode, we discuss his long journey in academia --- his PhD days at IIT Kanpur, 
teaching at Pondicherry University, his experiences teaching in France, and 
setting up institutes like NISER. We also touch upon the impact of computers on 
academia and, in particular, research in mathematics. 

Listen in to hear about his meandering journey as an Indian academic!

### Episode Notes:

[Ravi Shankar](https://en.wikipedia.org/wiki/Ravi_Shankar) 
and [Carnatic music](https://en.wikipedia.org/wiki/Carnatic_music)

[Madurai Kamaraj University](https://mkuniversity.ac.in/new/) 

[Marshall Stone](https://en.wikipedia.org/wiki/Marshall_Harvey_Stone) 
and [Stone--Weierstrass theorem](https://en.wikipedia.org/wiki/Stone%E2%80%93Weierstrass_theorem)

[Indian Statistical Institute (ISI), Kolkata](https://www.isical.ac.in/) 

[Indian Institute of Technology (IIT), Kanpur](https://www.iitk.ac.in/) 

[UB Tewari](https://en.wikipedia.org/wiki/Udai_Bhan_Tewari) 

[Henry Helson](https://en.wikipedia.org/wiki/Henry_Helson) 

[Mathematical Reviews](https://www.ams.org/mr-database) 

[Harmonic analysis](https://en.wikipedia.org/wiki/Harmonic_analysis) 

[Harish-Chandra](https://en.wikipedia.org/wiki/Harish-Chandra) 

[VS Varadarajan](https://en.wikipedia.org/wiki/Veeravalli_S._Varadarajan)

[Tata Institute of Fundamental Research (TIFR)](https://www.tifr.res.in/)

[Madras Presidency College](http://www.presidencycollegechennai.ac.in/)

[Weierstrass p-functions](https://en.wikipedia.org/wiki/Weierstrass%27s_elliptic_functions)

[Kronecker's theorem](https://en.wikipedia.org/wiki/Kronecker%27s_theorem)

[Stirling's formula](https://en.wikipedia.org/wiki/Stirling%27s_approximation)

[Four color theorem](https://en.wikipedia.org/wiki/Four_color_theorem)

[Harmonic series](https://en.wikipedia.org/wiki/Harmonic_series_(mathematics))

[Leonhard Euler](https://en.wikipedia.org/wiki/Leonhard_Euler)

[Georg Cantor](https://en.wikipedia.org/wiki/Georg_Cantor)

[Moore machine](https://en.wikipedia.org/wiki/Moore_machine), 
[Punch cards](https://en.wikipedia.org/wiki/Punched_card) and Intel processors 
([286](https://en.wikipedia.org/wiki/Intel_80286), 
 [386](https://en.wikipedia.org/wiki/Intel_80386), 
 and [Pentium](https://en.wikipedia.org/wiki/P5_(microarchitecture)))

[$$\LaTeX$$](https://en.wikipedia.org/wiki/LaTeX)

[David Hilbert](https://en.wikipedia.org/wiki/David_Hilbert)

[Felix Klein](https://en.wikipedia.org/wiki/Felix_Klein)

[University of Poitiers](https://www.univ-poitiers.fr/en/) 

[Pierre Deligne](https://en.wikipedia.org/wiki/Pierre_Deligne)

[Nicholas Varopoulos](https://en.wikipedia.org/wiki/Nicholas_Varopoulos)

[PV Narasimha Rao](https://en.wikipedia.org/wiki/P._V._Narasimha_Rao)

[Hyderabad University](https://uohyd.ac.in/), 
[Pondicherry University](https://www.pondiuni.edu.in/), 
[Benaras Hindu University](https://www.bhu.ac.in/)

[S Kumaresan](https://mtts.org.in/s-kumaresan/), 
[VS Sunder](https://en.wikipedia.org/wiki/V._S._Sunder), 
and [Mathematics Training and Talent Search Programme](https://mtts.org.in/)

[René Descartes](https://en.wikipedia.org/wiki/Ren%C3%A9_Descartes) 
and [Cartesian coordinates](https://en.wikipedia.org/wiki/Cartesian_coordinate_system)

[GH Hardy](https://en.wikipedia.org/wiki/G._H._Hardy)

[George Mallory](https://en.wikipedia.org/wiki/George_Mallory), 
[Tenzing Norgay](https://en.wikipedia.org/wiki/Tenzing_Norgay), 
and [Edmund Hillary](https://en.wikipedia.org/wiki/Edmund_Hillary)

[Jordan curve theorem](https://en.wikipedia.org/wiki/Jordan_curve_theorem)

[Thiruvalluvar](https://en.wikipedia.org/wiki/Thiruvalluvar)
