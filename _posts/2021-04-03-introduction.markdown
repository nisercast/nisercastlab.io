---
layout: post
title: "#0: Introduction"
date: 2021-04-03 00:00:01 +0530
audio: /assets/episodes/000.mp3
type: audio/mpeg
episodeType: trailer
length: 151 # duration of episode in seconds
---

NiSERCast is a podcast brought to you by NISER Science Activities Club; we 
invite scientists as guests and talk to them about their research and what life 
in academia is like. In this episode, Hradini and Ayush introduce the podcast 
and talk about the target audience.

### Episode Notes:

[National Institute of Science Education and Research](https://www.niser.ac.in)

[NiSERCast website]({{ site.url }})

